
variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "eu-central-1"
}

variable "aws_profile" {
  description = "AWS profile"
  type        = string
  default     = "devops"
}

variable "server_type" {
  default     = "t2.medium"
  type        = string
  description = "String"
}

variable "server_count" {
  default     = "1"
  type        = number
  description = "Number"
}

variable "termination_protection" {
  default     = false
  type        = bool
  description = "Bool"
}

# Define variables required for alarms

variable alarm_type {
  type        = list(string) 
  description = "Different Alarm Types"
}

variable evaluation_periods{
  type        = list(string) 
  description = "Evaluation Periods"
}

variable metric_name {
  type        = list(string) 
  description = "Metric Name"
}   

variable name_space {
  type        = list(string) 
  description = "Name Space"
}

variable period {
  type        = list(string) 
  description = "Period"
}

variable statistic {
  type        = list(string) 
  description = "Statistic"
}

variable threshold {
  type        = list(string) 
  description = "Threshold"
}

# Define instance ids

variable instance_id {
  type        = list(string) 
  description = "ID of each instance"
}

