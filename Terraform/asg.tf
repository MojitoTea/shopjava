resource "aws_launch_template" "my_server"{
name =  "my-server"
image_id = data.aws_ami.ubuntu.id
key_name = aws_key_pair.devcocer_key.key_name
user_data = base64encode(data.template_file.userdata.rendered)
vpc_security_group_ids = [aws_security_group.web_sg.id]
}

resource "aws_autoscaling_group" "my_server" {
 name = "my-server"
 min_size = 0
 max_size = 1
 health_check_type = "EC2"
 
 vpc_zone_identifier = module.vpc.private_subnets
 
 target_group_arns= [aws_lb_target_group.my_server.id]
 
 mixed_instances_policy {
  launch_template {
   launch_template_specification {
    launch_template_id = aws_launch_template.my_server.id
    }
    override {
     instance_type = "t2.medium"
     }
    }
  }
  
  timeouts {
    delete = "15m"
  }
}

resource "aws_autoscaling_policy" "my_server" {
 name = "my-server"
 policy_type = "TargetTrackingScaling"
 autoscaling_group_name = aws_autoscaling_group.my_server.name
 
 estimated_instance_warmup = 300
 
 target_tracking_configuration {
  predefined_metric_specification {
   predefined_metric_type = "ASGAverageCPUUtilization"
  }
  
  target_value = 90.0
 }
}
