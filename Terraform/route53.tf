data "aws_route53_zone" "public" {
 name = "avenganik.pp.ua" 
 private_zone = false
}

#resource "aws_acm_certificate" "api" {
# domain_name = "avenganik.pp.ua"
# validation_method = "DNS"
# }
 
 #resource "aws_route53_record" "api_validation" {
 # for_each = {
 #  for dvo in aws_acm_certificate.api.domain_validation_options : dvo.domain_name => {
 #    name = dvo.resource_record_name
 #    record = dvo.resource_record_value
 #    type = dvo.resource_record_type
 #   }
 #  }
   
 #  allow_overwrite = true
 #  name = each.value.name
 #  records = [each.value.record]
 #  ttl = 60
 #  type = each.value.type
 #  zone_id = data.aws_route53_zone.public.zone_id
 #}

#resource "aws_acm_certificate_validation" "api" {
# certificate_arn = aws_acm_certificate.api.arn
# validation_record_fqdns = [for record in aws_route53_record.api_validation : record.fqdn]
# timeouts {
# create = "15m"
# }
#}

resource "aws_route53_record" "api" {
 name = "avenganik.pp.ua" 
 type = "A"
 zone_id = data.aws_route53_zone.public.zone_id
 
 alias {
  name = aws_lb.my_server.dns_name
  zone_id = aws_lb.my_server.zone_id
  evaluate_target_health = false
  }
 }









