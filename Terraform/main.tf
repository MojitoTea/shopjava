provider "aws" {
 region  = var.aws_region
 profile = var.aws_profile
}

# MODULES BLOCK

module "db" {
  source = "terraform-aws-modules/rds/aws"

  identifier                     = "complete-postgresql"


  create_db_option_group    = false
  create_db_parameter_group = false

  # All available versions: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_PostgreSQL.html#PostgreSQL.Concepts
  engine               = "postgres"
  engine_version       = "10.20"
  family               = "postgres10.20" # DB parameter group
  major_engine_version = "10.20"         # DB option group
  instance_class       = "db.t3.micro"

  allocated_storage = 10

  # NOTE: Do NOT use 'user' as the value for 'username' as it throws:
  # "Error creating DB Instance: InvalidParameterValue: MasterUsername
  # user cannot be used as it is a reserved word used by the engine"
  db_name  = "avengadb"
  username = "postgres"
  password = "rootroot1"
  port     = 5432
  create_random_password = false
  db_subnet_group_name   = module.vpc.database_subnet_group
  vpc_security_group_ids = [aws_security_group.db_sg.id]
  
  maintenance_window      = "Mon:00:00-Mon:03:00"
  backup_window           = "03:00-06:00"
  backup_retention_period = 1


  tags = {
  Name = "Db_PostgresAvenga"
  Owner = "Nikita"
  Environment = "Production"
  }
}




module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"

  name = "VPC_Avenga"
  cidr = "10.99.0.0/18"

  azs              = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  public_subnets   = ["10.99.0.0/24", "10.99.1.0/24", "10.99.2.0/24"]
  private_subnets  = ["10.99.3.0/24", "10.99.4.0/24", "10.99.5.0/24"]
  database_subnets = ["10.99.7.0/24", "10.99.8.0/24", "10.99.9.0/24"]

  create_database_subnet_group       = true
  create_database_subnet_route_table = true

 tags = {
  Name = "vpc_avenga"
  Owner = "Nikita"
  Environment = "Production"
  }
}


#################

data "template_file" "userdata" {
  template = file("./scripts/install.sh")
  vars = {
    vpc_id = module.vpc.vpc_id
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"]
}


resource "aws_instance" "server" {
  count                   = var.server_count
  disable_api_termination = var.termination_protection
  instance_type           = var.server_type
  ami                     = data.aws_ami.ubuntu.id
  vpc_security_group_ids  = [aws_security_group.web_sg.id]
  subnet_id               = element(module.vpc.public_subnets, 0) 
  availability_zone       = element(module.vpc.azs, 0)
  associate_public_ip_address = true
  user_data = data.template_file.userdata.rendered

  key_name                = aws_key_pair.devcocer_key.key_name
    tags = {
    Name = "webserv"
  }
}

#resource "aws_instance" "server-monitor" {
#  count                   = var.server_count
#  disable_api_termination = var.termination_protection
#  instance_type           = var.server_type
#  ami                     = data.aws_ami.ubuntu.id
#  vpc_security_group_ids  = [aws_security_group.web_sg.id]
#  subnet_id               = element(module.vpc.public_subnets, 1) 
#  availability_zone       = element(module.vpc.azs, 1)
#  associate_public_ip_address = true

#  key_name                = aws_key_pair.devcocer_key.key_name
#    tags = {
#    Name = "monitoring"
#  }
#}



resource "aws_key_pair" "devcocer_key" {
    key_name   = "devcocer-key"
    public_key = file("~/.ssh/devcocs.pub")
  }
