terraform {
  backend "s3" {
    bucket         = "remoteback-2022nikita228"
    key            = "remoteback/terraform.tfstate"
    dynamodb_table = "remotebacken228mojitotea"
    encrypt        = true   
    region = "eu-central-1"
    profile = "devops"
  }
}
