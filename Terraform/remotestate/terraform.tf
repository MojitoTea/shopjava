terraform {
  backend "s3" {
    bucket         = "stateback-end3478976"
    key            = "remoteback/terraform.tfstate"
    dynamodb_table = "remotebacken20202aveng"
    encrypt        = true   
    region = "eu-central-1"
    profile = "devops"
  }
}
