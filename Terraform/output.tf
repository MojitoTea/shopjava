output "server_ip" {
  value = aws_instance.server[0].public_ip
}

output "server_dns" {
  value = "http://${aws_instance.server[0].public_dns}"
}
