resource "aws_lb_target_group" "my_server"{
 name = "my-server"
 port = 8080
 protocol = "HTTP"
 vpc_id = module.vpc.vpc_id
 
  health_check {
   enabled = true
   port = 8081
   interval = 30
   protocol = "HTTP"
   path = "/health"
   matcher = "200"
   healthy_threshold = 3
   unhealthy_threshold = 3
   }
}

resource "aws_lb" "my_server"{
 name               = "my-server"
 internal           = false
 load_balancer_type = "application"
 security_groups    = [aws_security_group.alb_sg.id]
 subnets            = module.vpc.public_subnets
 
 }
 
 resource "aws_lb_listener" "my_server" {
 load_balancer_arn = aws_lb.my_server.arn
 port              = "80"
 protocol          = "HTTP"
 
 default_action {
  type = "forward"
  target_group_arn = aws_lb_target_group.my_server.arn
 }
 }
 
 
 ###################################
 
 
 #resource "aws_lb_listener" "my_server_tls" {
 #load_balancer_arn = aws_lb.my_server.arn
 #port              = "443"
 #protocol          = "HTTPS"
 #certificate_arn   = aws_acm_certificate.api.arn
 #ssl_policy        = "ELBSecurityPolicy-2016-08"
 
 #default_action {
 # type = "forward"
 # target_group_arn = aws_lb_target_group.my_server.arn
 # }
  
 # depends_on = [aws_acm_certificate_validation.api]
 #}
 
 
 
 
