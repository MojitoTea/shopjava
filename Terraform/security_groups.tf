resource "aws_security_group" "web_sg" {
    name        = "web_sg"
    vpc_id      = module.vpc.vpc_id
    tags = {
      "Name"    = "web_sg"
    }
  
}

resource "aws_security_group_rule" "allow_ssh" {
    type = "ingress"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.web_sg.id
}

resource "aws_security_group_rule" "allow_out" {
    type = "egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.web_sg.id
}
resource "aws_security_group_rule" "allow_http" {
    type = "ingress"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.web_sg.id
}
resource "aws_security_group_rule" "allow_https" {
    type = "ingress"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.web_sg.id
}
####alb+ec2
resource "aws_security_group_rule" "ingress_trafic_web" {
    type = "ingress"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_group_id = aws_security_group.web_sg.id
    source_security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "ingress_trafic_web_healthcheck" {
    type = "ingress"
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    security_group_id = aws_security_group.web_sg.id
    source_security_group_id = aws_security_group.alb_sg.id
}



######
# DB Security Group of BD
resource "aws_security_group" "db_sg" {
    name        = "db_sg"
    vpc_id      = module.vpc.vpc_id

    tags = {
      "Name"    = "db-sg"
    }
  
}

# DB Securoty Group Rules
resource "aws_security_group_rule" "allow_db" {
    type = "ingress"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [module.vpc.vpc_cidr_block]
    security_group_id = aws_security_group.db_sg.id
}

resource "aws_security_group_rule" "allow_db_out" {
    type = "egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.db_sg.id
}


#####################
# ALB Security Group of ALB
resource "aws_security_group" "alb_sg" {
    name        = "alb_sg"
    vpc_id      = module.vpc.vpc_id

    tags = {
      "Name"    = "alb-sg"
    }
  
}

resource "aws_security_group_rule" "allow_http_alb" {
    type = "ingress"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "allow_https_alb" {
    type = "ingress"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "egress_alb_web" {
    type = "egress"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_group_id = aws_security_group.alb_sg.id
    source_security_group_id = aws_security_group.web_sg.id
}

resource "aws_security_group_rule" "egress_alb_web_health" {
    type = "egress"
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    security_group_id = aws_security_group.alb_sg.id
    source_security_group_id = aws_security_group.web_sg.id
}



